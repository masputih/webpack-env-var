const webpack = require('webpack')
const path = require('path');

module.exports = env => {
  const envjs = env === 'prod' ? 'production.env.js' : 'dev.env.js';
  const out = env === 'prod' ? 'prod' : 'dev';

  console.log('ENV:',env);

  return {
    entry: path.resolve(__dirname, 'src/index.js'),
    resolve: {
      extensions: ['.js'],
      alias: {
        'env-config$': path.resolve(__dirname, envjs)
      }
    },
    output: {
      path: path.resolve(__dirname, 'build', out),
      filename: "bundle.js"
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
          options: {
            presets: ['es2015']
          }
        }
      ]
    }
  }
}